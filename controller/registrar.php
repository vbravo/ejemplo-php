<?php  
/*
obligo a este fichero
a usar mis datos de
acceso a la base de datos
*/
require 'conexion.php';

//antes de nada obtengo la contraseña y la cifro para insertarla
$clave = $_POST['clave'];

//y ahora cifro la clave usando un hash
$clave_cifrada = password_hash($clave, PASSWORD_DEFAULT, array("cost"=>15));
echo $clave_cifrada;
/*
el "cost" lo que indica es la fuerza con la que se cifra la clave
a mayor fuerza mas tiempo de carga requiere la página por eso es
importante que busques un equilibrio
*/

/*
compruebo que la conexion es correcta
y de ser asi hago el insert
*/
if ($conexion == true) {

 //preparo el insert...
 $insert = $conexion->prepare("INSERT INTO users (email,password) VALUES (:email,:clave)");

 //asocio los campos del insert a los campos del formulario

 $insert->bindParam(':email', $_POST['email']);

 $insert->bindParam(':clave', $clave_cifrada);


 //si no he echo el imbecil y todo esta correcto ejecuto lo anterior
 $insert->execute();

 //cierro la conexion
 $conexion = null;

 //redirijo a otro archivo php
 header("Location: ../index.html");
} else {
 echo "Algo ha fallado bro";
}
?>